package de.tantalos.itemset.connector;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.bukkit.entity.Player;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Preconditions;

import de.tantalos.itemset.ItemSetManager;
import de.tantalos.itemset.configuration.ConfigurationData;
import de.tantalos.itemset.configuration.ItemData;
import de.tantalos.itemset.configuration.ItemSetData;
import de.tantalos.itemset.configuration.ItemSlotData;
import de.tantalos.itemset.configuration.PlayerFilter;
import de.tantalos.itemset.configuration.PlayerPredicate;
import de.tantalos.itemset.configuration.XMLConfigHelper;
import junit.framework.Assert;

public class XMLModelIntegrationTest {

    private ConfigurationData itemset;
    private InputStream testIS;

    @Before
    public void setup() throws IllegalConfigurationException, IOException {
        XMLConfigHelper config = getMockedConfigHelper("/complex_config.xml");

        ItemSetManager itemManager = mock(ItemSetManager.class);

        when(itemManager.loadConfigFrom(any(XMLConfigHelper.class))).thenCallRealMethod();
        itemset = itemManager.loadConfigFrom(config);
    }

    private XMLConfigHelper getMockedConfigHelper(String configName) throws IOException {
        URL resourceURL = XMLModelIntegrationTest.class.getResource(configName);
        testIS = resourceURL.openStream();

        XMLConfigHelper config = mock(XMLConfigHelper.class);
        when(config.loadConfigAsStream()).thenReturn(testIS);

        return config;
    }

    @Test
    public void testFullProfileToPOJO() throws JAXBException, IllegalConfigurationException {
        ItemSetData profile = itemset.getItemSet().get(0);
        Preconditions.checkArgument("full_profile".equals(profile.getName()));

        assertFullProfile(profile);
    }

    @Test
    public void testMinimalProfileToPOJO() throws JAXBException, IllegalConfigurationException {
        for (ItemSetData profile : itemset.getItemSet()) {
            assertMinimalProfile(profile);
        }
    }

    private static void assertMinimalProfile(ItemSetData profile) {
        Assert.assertNotNull(profile);
        Assert.assertNotNull(profile.getWorld());
    }

    private static void assertFullProfile(ItemSetData profile) {
        assertMinimalProfile(profile);

        Assert.assertNotNull(profile.getItemSlots());
        PlayerPredicate playerFilter = profile.getPlayerFilter();
        Assert.assertNotNull(playerFilter);
    }

    @Test
    public void testSingleItemSlotToPOJO() throws JAXBException, IllegalConfigurationException {
        ItemSetData profile = itemset.getItemSet().get(0);
        assumeProfileIsCorrect(profile);

        List<ItemSlotData> itemSlots = profile.getItemSlots();
        Assert.assertNotNull(itemSlots);

        for (ItemSlotData itemSlot : profile.getItemSlots()) {
            assertItemSlot(itemSlot);
        }
    }

    private static void assertItemSlot(ItemSlotData itemSlot) {
        Assert.assertNotNull(itemSlot.getPosition());
        Assert.assertNotNull(itemSlot.getItem());
    }

    @Test
    public void testMinimalItemToPOJO() throws IllegalConfigurationException {
        ItemSetData profile = itemset.getItemSet().get(0);
        assumeProfileIsCorrect(profile);

        for (ItemSlotData itemSlot : profile.getItemSlots()) {
            ItemData item = itemSlot.getItem();
            assertMinimalItem(item);
        }
    }

    @Test
    public void testFullItemToPOJO() throws IllegalConfigurationException {
        ItemSetData profile = itemset.getItemSet().get(0);
        assumeProfileIsCorrect(profile);

        ItemData item = profile.getItemSlots().get(0).getItem();
        Preconditions.checkArgument("full-item".equals(item.getId()));
        assertMinimalItem(item);
        Assert.assertNotNull(item.getArgs());
    }

    private static void assertMinimalItem(ItemData item) {
        Assert.assertNotNull(item);
        Assert.assertNotNull(item.getId());
        Assert.assertNotNull(item.getItemFactoryName());
    }

    private static void assumeProfileIsCorrect(ItemSetData profile) {
        Assume.assumeNotNull(profile);
    }

    @Test
    public void testTautologyFilter() {
        Player opPlayer = getPlayerMock(true);
        Player normalPlayer = getPlayerMock(false);

        PlayerFilter filter = getFilterByPredefinedName("tautology", itemset);
        Assert.assertTrue(filter.apply(opPlayer));
        Assert.assertTrue(filter.apply(normalPlayer));
    }

    @Test
    public void testContradictionFilter() {
        Player opPlayer = getPlayerMock(true);
        Player normalPlayer = getPlayerMock(false);

        PlayerFilter filter = getFilterByPredefinedName("contradiction", itemset);
        Assert.assertFalse(filter.apply(opPlayer));
        Assert.assertFalse(filter.apply(normalPlayer));
    }

    @Test
    public void testDoubleNotFilter()
            throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
        // random values. a verifier would be nice
        Player opPlayer = getPlayerMock(true);
        Player normalPlayer = getPlayerMock(false);

        PlayerFilter filter = getFilterByPredefinedName("double-not", itemset);
        PlayerPredicate filterPredicate = filter.getPlayerPredicate();

        Field f = filterPredicate.getClass().getDeclaredField("negative");
        f.setAccessible(true);
        PlayerPredicate innerFilterPredicate = (PlayerPredicate) f.get(filterPredicate);

        Preconditions.checkArgument(filterPredicate.apply(opPlayer));
        Assert.assertFalse(innerFilterPredicate.apply(opPlayer));

        Preconditions.checkArgument(!filterPredicate.apply(normalPlayer));
        Assert.assertTrue(innerFilterPredicate.apply(normalPlayer));
    }

    @Test
    public void testLiterals()
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        PlayerFilter filter = getFilterByPredefinedName("every-literal", itemset);

        PlayerPredicate pred = filter.getPlayerPredicate();

        Field f = pred.getClass().getDeclaredField("disjuction");
        f.setAccessible(true);
        @SuppressWarnings("unchecked")
        List<PlayerPredicate> allLiteralFilter = (List<PlayerPredicate>) f.get(pred);

        for (PlayerPredicate predicate : allLiteralFilter) {
            Assert.assertNotNull(predicate);
        }
    }

    private Player getPlayerMock(boolean op) {
        Player player = mock(Player.class);
        when(player.isOp()).thenReturn(op);
        return player;
    }

    private static PlayerFilter getFilterByPredefinedName(String filterId, ConfigurationData itemset) {
        List<PlayerFilter> allFilter = itemset.getFilter();

        for (PlayerFilter filter : allFilter) {
            if (filterId.equals(filter.getId())) {
                return filter;
            }
        }
        return null;
    }

    @After
    public void closeStream() throws IOException {
        testIS.close();
    }

}
