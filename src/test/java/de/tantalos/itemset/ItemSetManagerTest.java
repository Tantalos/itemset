package de.tantalos.itemset;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;

import org.bukkit.Server;
import org.bukkit.plugin.Plugin;
import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import de.tantalos.itemset.configuration.ConfigurationData;
import de.tantalos.itemset.configuration.XMLConfigHelper;
import de.tantalos.itemset.connector.IllegalConfigurationException;
import de.tantalos.itemset.connector.ItemBuilder;
import de.tantalos.itemset.connector.XMLModelIntegrationTest;

/*
 * If tests fail, check XMLModelIntegrationTest first
 */
public class ItemSetManagerTest {

    private InputStream testIS;
    private ItemSetManager itemManager;
    // private static Logger logger = mock(Logger.class);

    @Before
    public void setup() throws IllegalConfigurationException, IOException {
        Plugin pluginMock = mock(Plugin.class);
        Server serverMock = mock(Server.class);

        when(pluginMock.getServer()).thenReturn(serverMock);
        itemManager = spy(new ItemSetManager(pluginMock));

    }

    private ConfigurationData getConfig(XMLConfigHelper config) {
        ConfigurationData itemset = null;
        try {
            ItemSetManager itemManager = mock(ItemSetManager.class);
            when(itemManager.loadConfigFrom(any(XMLConfigHelper.class))).thenCallRealMethod();
            itemset = itemManager.loadConfigFrom(config);
        } catch (Throwable e) {
            /* Configuration is tested in XMLModelIntegrationTest */
            Assume.assumeTrue(false);
        }
        return itemset;
    }

    private XMLConfigHelper getMockedConfigHelper(String configName) throws IOException {
        URL resourceURL = XMLModelIntegrationTest.class.getResource(configName);
        testIS = resourceURL.openStream();

        XMLConfigHelper config = mock(XMLConfigHelper.class);
        when(config.loadConfigAsStream()).thenReturn(testIS);

        return config;
    }

    @After
    public void closeStream() throws IOException {
        testIS.close();
    }

    @Test
    public void testItemInitialization() throws IOException, IllegalConfigurationException {
        XMLConfigHelper config = getMockedConfigHelper("/single_item_config.xml");
        ConfigurationData itemset = getConfig(config);
        Assume.assumeNotNull(itemset);

        itemManager.createAllItemBuilder(itemset);

        Map<String, ItemBuilder> items = itemManager.getItems();

        Assert.assertNotNull(items.size() == 1);

        ItemBuilder singleItem = items.get("item");
        Assume.assumeNotNull(singleItem);

    }

    @Test(timeout = 5000, expected = IllegalConfigurationException.class)
    public void testReciprocalItemContainer() throws IllegalConfigurationException, IOException {
        XMLConfigHelper config = getMockedConfigHelper("/reciprocal_config.xml");
        ConfigurationData itemset = getConfig(config);
        Assume.assumeNotNull(itemset);

        itemManager.createAllItemBuilder(itemset);
    }

    @Test
    public void testItemBadOrder() throws IOException, IllegalConfigurationException {
        XMLConfigHelper config = getMockedConfigHelper("/bad_order_config.xml");
        ConfigurationData itemset = getConfig(config);
        Assume.assumeNotNull(itemset);

        itemManager.createAllItemBuilder(itemset);
        Map<String, ItemBuilder> items = itemManager.getItems();

        Assert.assertTrue(items.size() == 3);
    }

    @Test
    public void testNestedContainerItem() throws IOException, IllegalConfigurationException {
        XMLConfigHelper config = getMockedConfigHelper("/nested_container_config.xml");
        ConfigurationData itemset = getConfig(config);
        Assume.assumeNotNull(itemset);

        itemManager.createAllItemBuilder(itemset);
        Map<String, ItemBuilder> items = itemManager.getItems();

        Assert.assertTrue(items.size() == 4);
    }
}
