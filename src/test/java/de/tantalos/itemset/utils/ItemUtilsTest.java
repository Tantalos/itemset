package de.tantalos.itemset.utils;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.junit.Test;

public class ItemUtilsTest {

    @Test
    public void testColoredItem() {
        for (Material material : ItemUtils.colorableBlocks) {
            ItemUtils.getColoredBlock(material, DyeColor.BLUE);
        }

    }
}
