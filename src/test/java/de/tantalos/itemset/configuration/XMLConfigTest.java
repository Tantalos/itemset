package de.tantalos.itemset.configuration;

import java.io.File;
import java.io.IOException;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Preconditions;

/**
 * Uses Preconditions instead of Assume. The test will fail if it is not viable
 *
 */
public class XMLConfigTest {

    private XMLConfigHelper config;

    @Before
    public void setup() {
        config = new XMLConfigHelper("target/temp-test/");
    }

    @Test
    public void testDefaultCopy() throws IOException {
        Preconditions.checkArgument(!new FileExists().matches(config.CONFIG_LOCATION),
                "Files exists before writing. Test ist not meaningful");
        Preconditions.checkArgument(!new FileExists().matches(config.DTD_LOCATION),
                "Files exists before writing. Test ist not meaningful");

        config.saveDefaultConfig();

        Assert.assertThat(config.CONFIG_LOCATION, new FileExists());
        Assert.assertThat(config.DTD_LOCATION, new FileExists());
    }

    @After
    public void cleanup() {
        config.CONFIG_LOCATION.delete();
        config.DTD_LOCATION.delete();

    }

    public class FileExists extends BaseMatcher<File> {

        @Override
        public boolean matches(Object file) {
            if (!(file instanceof File)) {
                return false;
            }
            return ((File) file).exists();
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("file must exists");
        }


    }
}
