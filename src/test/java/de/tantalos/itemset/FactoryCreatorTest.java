package de.tantalos.itemset;

import org.junit.Test;

import de.tantalos.itemset.connector.IllegalConfigurationException;
import de.tantalos.itemset.connector.ItemBuilder;
import de.tantalos.itemset.connector.ItemFactory;
import de.tantalos.itemset.utils.StaticItemFactory;
import junit.framework.Assert;

public class FactoryCreatorTest {

    @Test
    public void testStaticItemFactory() throws IllegalConfigurationException, ReflectiveOperationException {
        @SuppressWarnings("unchecked")
        ItemFactory<ItemBuilder> staticItemFactory = (ItemFactory<ItemBuilder>) ItemFactoryCreator
                .createItemFactoryViaReflection(StaticItemFactory.class.getName(), null);

        Assert.assertNotNull(staticItemFactory.buildItemDefinition());
    }
}
