package de.tantalos.itemset.utils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;

public class ItemUtils {
    final static Collection<Material> colorableBlocks = Sets.newHashSet();
    private static final Random random = new Random();


    static {
        colorableBlocks.add(Material.STAINED_GLASS_PANE);
        colorableBlocks.add(Material.STAINED_GLASS);
        colorableBlocks.add(Material.STAINED_CLAY);
        colorableBlocks.add(Material.WOOL);
        colorableBlocks.add(Material.INK_SACK);
    }

    public static ItemStack createItem(Material material, String... text) {

        ItemStack item = new ItemStack(material);
        return addText(item, text);
    }

    public static ItemStack addText(ItemStack item, String... text) {
        if (text != null && text.length <= 0) {
            return item;
        }
        ItemMeta meta = item.getItemMeta();
        if (text.length >= 1) {
            meta.setDisplayName(text[0]);
        }
        if (text.length > 1) {
            ArrayList<String> lore = new ArrayList<String>();
            for (int i = 1; i < text.length; i++) {
                lore.add(text[i]);
            }
            meta.setLore(lore);
        }
        item.setItemMeta(meta);

        return item;
    }

    public static ItemStack createItem(Material material) {
        return new ItemStack(material);
    }

    @SuppressWarnings("deprecation")
    public static ItemStack getColoredBlock(Material material, DyeColor color) {
        Preconditions.checkArgument(colorableBlocks.contains(material));
        return new ItemStack(material, 1, color.getWoolData());
    }

    /**
     * look http://minecraft.gamepedia.com/Data_values/Entity_IDs for ids
     *
     * @return
     */
    public static ItemStack getMosnterEgg(short id) {
        ItemStack monsterEgg = new ItemStack(Material.MONSTER_EGG, 1, id);
        return monsterEgg;
    }

    /**
     * Note: if you don't set the id by your self, there is a minor likelihood
     * for an id collision
     *
     * @return item with invisible identifier lore
     */
    public static ItemStack makeItemStackIdentifiable(ItemStack item) {
        String identifier = new BigInteger(130, random).toString(32);

        return makeItemStackIdentifiable(item, identifier);
    }

    /**
     *
     * @return item with invisible identifier lore
     */
    public static ItemStack makeItemStackIdentifiable(ItemStack item, String id) {
        final String invisibleIdentifier = createInvisibleString(id);

        ItemMeta meta = item.getItemMeta();
        List<String> lore = meta.getLore();

        /*
         * add invisible identifier string. This string will be checked in
         * equals/isSimilar methods
         */
        lore.add(invisibleIdentifier);
        meta.setLore(lore);
        item.setItemMeta(meta);

        return item;
    }

    private static String createInvisibleString(String string) {
        StringBuilder b = new StringBuilder();
        for (Character c : string.toCharArray()) {
            b.append(String.valueOf(ChatColor.COLOR_CHAR));
            b.append(c.toString());
        }
        return b.toString();
    }

}
