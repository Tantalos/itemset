package de.tantalos.itemset.utils;

import org.bukkit.Material;

import de.tantalos.itemset.connector.IllegalConfigurationException;
import de.tantalos.itemset.connector.ItemBuilder;
import de.tantalos.itemset.connector.ItemFactory;

public class StaticItemFactory implements ItemFactory<ItemBuilder> {

    @Override
    public ItemBuilder buildItemDefinition(String... args) throws IllegalConfigurationException {
        Material material = Material.STICK;
        int amount = 1;
        String text = null;
        for (String arg : args) {
            String key = ItemFactory.getKey(arg);
            String value = ItemFactory.getValue(arg);

            switch (key) {
            case "material":
                material = Material.valueOf(value);
                break;
            case "amount":
                amount = Integer.valueOf(value);
                break;
            case "text":
                text = value;
                break;
            default:
                // nothing
            }
        }

        return new StaticItem(material, amount, text);
    }

}
