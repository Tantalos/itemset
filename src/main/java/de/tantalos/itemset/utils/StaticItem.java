package de.tantalos.itemset.utils;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.base.Preconditions;

import de.tantalos.itemset.connector.ItemBuilder;

public class StaticItem implements ItemBuilder {

    private final Material itemMaterial;
    private final int amount;
    private final String[] text;

    public StaticItem(Material itemMaterial, String... text) {
        this(itemMaterial, 1, text);
    }

    public StaticItem(Material itemMaterial, int amount, String... text) {
        Preconditions.checkArgument(amount <= 64);
        Preconditions.checkArgument(amount > 0);

        this.itemMaterial = itemMaterial;
        this.amount = amount;
        this.text = text;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(itemMaterial, amount);
        if (text == null || text.length <= 0) {
            return item;
        }
        ItemMeta meta = item.getItemMeta();
        if (text.length >= 1) {
            meta.setDisplayName(text[0]);
        }
        if (text.length > 1) {
            ArrayList<String> lore = new ArrayList<String>();
            for (int i = 1; i < text.length; i++) {
                lore.add(text[i]);
            }
            meta.setLore(lore);
        }
        item.setItemMeta(meta);

        return item;
    }

}
