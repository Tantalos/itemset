package de.tantalos.itemset.utils;

import java.util.List;
import java.util.Random;

import org.bukkit.inventory.ItemStack;

import de.tantalos.itemset.connector.IllegalConfigurationException;
import de.tantalos.itemset.connector.ItemBuilder;
import de.tantalos.itemset.connector.ItemContainerFactory;

public class RandomItemFactory extends ItemContainerFactory<ItemBuilder> {

    public RandomItemFactory(List<ItemBuilder> nestedItems) {
        super(nestedItems);
    }

    @Override
    public ItemBuilder buildItemDefinition(String... args) throws IllegalConfigurationException {
        if (nestedItems.size() < 1) {
            throw new IllegalConfigurationException("Random item factory needs at least one item to choose from");
        }

        return new ItemBuilder() {

            @Override
            public ItemStack getItem() {
                int size = nestedItems.size();
                Random random = new Random();
                int randomIndex = random.nextInt(size);

                return nestedItems.get(randomIndex).getItem();
            }
        };
    }

}
