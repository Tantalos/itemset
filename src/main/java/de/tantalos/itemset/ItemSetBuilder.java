package de.tantalos.itemset;

import java.util.Collection;
import java.util.Map;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import de.tantalos.itemset.configuration.IsPlayerPredicate;
import de.tantalos.itemset.configuration.PlayerPermissionPredicate;
import de.tantalos.itemset.connector.ItemBuilder;

public class ItemSetBuilder {

    private World world = null;
    private String permission = null;
    private Collection<Player> players = Sets.newHashSet();
    private Predicate<Player> playerPredicate = Predicates.alwaysTrue();
    private final Plugin plugin;
    private final Map<Integer, ItemBuilder> itemDefinition = Maps.newHashMap();

    public ItemSetBuilder(Plugin plugin) {
        Preconditions.checkNotNull(plugin);
        this.plugin = plugin;
    }

    public ItemSetBuilder setWorld(World world) {
        this.world = world;
        return this;
    }

    public ItemSetBuilder setPermission(String permission) {
        this.permission = permission;
        return this;
    }

    public ItemSetBuilder setPlayer(Collection<Player> players) {
        Preconditions.checkNotNull(players);
        this.players = players;
        return this;
    }

    public ItemSetBuilder addPlayer(Player player) {
        players.add(player);
        return this;
    }

    /**
     * Sets a predicate which must be fulfilled to allow world set functionality
     * to a specified player group.
     *
     * @param playerPredicate
     * @return
     */
    public ItemSetBuilder setPlayerPredicate(Predicate<Player> playerPredicate) {
        this.playerPredicate = playerPredicate;
        return this;
    }

    /**
     * slot 0-8 are the main bottom slots from left to right. Slot 9-35 are the
     * other inventory slots from left to right from TOP! to bottom. Slot 36-39
     * is helmet to boots. Slot 40 is off hand
     *
     * @param slot
     * @param itemKey
     */
    public ItemSetBuilder fixSlot(int slot, ItemBuilder itemKey) {
        Preconditions.checkNotNull(itemKey);
        Preconditions.checkArgument(slot < 41);

        itemDefinition.put(slot, itemKey);
        return this;
    }


    /**
     * Builds predicate for which users get the items at start
     *
     * @return
     */
    private Predicate<Player> buildPredicate() {
        Predicate<Player> predicate = Predicates.alwaysTrue();

        if (permission != null) {
            predicate = Predicates.and(predicate, new PlayerPermissionPredicate(permission));
        }


        if (players != null && !players.isEmpty()) {
            for(Player player : players) {
                predicate = Predicates.and(predicate, new IsPlayerPredicate(player));
            }
        }

        return predicate;
    }

    public ItemSet build() {
        if (world == null) {
            this.world = plugin.getServer().getWorld("world");
        }

        Predicate<Player> worldSetPredicate = Predicates.<Player>and(this.playerPredicate, buildPredicate());

        return new ItemSet(plugin, world, worldSetPredicate, itemDefinition);
    }
}
