package de.tantalos.itemset.connector;

import java.util.function.Function;

import org.bukkit.inventory.ItemStack;

/**
 * Creates an itemStack
 *
 */
public interface ItemBuilder extends Function<Void, ItemStack> {

    @Override
    default ItemStack apply(Void t) {
        return getItem();
    }

    public ItemStack getItem();
}
