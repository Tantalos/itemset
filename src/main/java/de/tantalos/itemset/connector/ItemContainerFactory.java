package de.tantalos.itemset.connector;

import java.util.List;

import com.google.common.collect.ImmutableList;

public abstract class ItemContainerFactory<T extends ItemBuilder> implements ItemFactory<ItemBuilder> {
    protected final ImmutableList<? extends ItemBuilder> nestedItems;

    public ItemContainerFactory(List<? extends ItemBuilder> nestedItems) {
        this.nestedItems = ImmutableList.<ItemBuilder>copyOf(nestedItems);
    }
}
