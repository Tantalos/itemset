package de.tantalos.itemset.connector;

public class IllegalConfigurationException extends Exception {

    private static final long serialVersionUID = 1L;

    public IllegalConfigurationException() {

    }

    public IllegalConfigurationException(String message) {
        super(message);
    }

    public IllegalConfigurationException(Throwable cause) {
        super(cause);
    }

    public IllegalConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

}
