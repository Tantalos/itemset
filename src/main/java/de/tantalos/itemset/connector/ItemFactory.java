package de.tantalos.itemset.connector;

/**
 * Creates an item builder which is setup with given dynamic arguments
 *
 * @param <T>
 *            an item builder
 */
public interface ItemFactory<T extends ItemBuilder> {

    public abstract T buildItemDefinition(String... args) throws IllegalConfigurationException;

    static String getKey(String arg) throws IllegalConfigurationException {

        try {
            return arg.split("=")[0];
        } catch (NullPointerException e) {
            throw new IllegalConfigurationException("Illegal configured argument: " + arg);
        }
    }

    static String getValue(String arg) throws IllegalConfigurationException {
        try {
            return arg.split("=")[1];
        } catch (NullPointerException e) {
            throw new IllegalConfigurationException("Illegal configured argument: " + arg);
        }
    }
}
