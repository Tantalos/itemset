package de.tantalos.itemset;

import java.util.Iterator;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.ImmutableMap;

import de.tantalos.itemset.connector.ItemBuilder;

public class ItemSetCommands implements CommandExecutor {

    private final ImmutableMap<String, ItemBuilder> configuredItems;
    private final Server server;

    public ItemSetCommands(Map<String, ItemBuilder> configuredItems, Server server) {
        this.configuredItems = ImmutableMap.<String, ItemBuilder>copyOf(configuredItems);
        this.server = server;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            return false;
        }

        switch (args[0]) {
        case "show":
            return show(sender, label, args);
        case "give":
            return give(sender, label, args);
        default:
            return false;
        }
    }

    private boolean give(CommandSender sender, String label, String[] args) {
        if (!sender.hasPermission("itemset.command.give")) {
            sendNoPermission(sender);
            return true;
        }

        if (args.length < 2) {
            return false;
        }

        String itemName = args[1];
        ItemBuilder itemBuilder = configuredItems.get(itemName);
        if (itemBuilder == null) {
            sender.sendMessage(ChatColor.RED + "There is no item with name " + itemName
                    + " loaded. Use  /itemset show  to see all items");
            return true;
        }

        /* player who gets item */
        Player player = null;
        if(args.length > 2) {
            String playerName = args[2];
            player = server.getPlayer(playerName);
            if (player == null) {
                sender.sendMessage(ChatColor.RED + "The specified player " + playerName + " is unknown");
                return true;
            }
        } else {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "You need to specify player who gets the item");
                return true;
            }
            player = (Player) sender;
        }

        assert player != null;
        ItemStack item = itemBuilder.getItem();
        player.getInventory().addItem(item);
        return true;
    }

    private boolean show(CommandSender sender, String label, String[] args) {
        if (!sender.hasPermission("itemset.command.show")) {
            sendNoPermission(sender);
            return true;
        }
        String message = buildItemInfoString();
        sender.sendMessage(message);
        return true;
    }

    private void sendNoPermission(CommandSender sender) {
        sender.sendMessage(ChatColor.RED + "You don't have permission for that");
    }

    private String buildItemInfoString() {
        StringBuilder b = new StringBuilder();
        b.append(ChatColor.GREEN);
        b.append("---------The following items are configured---------\n");
        b.append(ChatColor.YELLOW);

        Iterator<String> it = configuredItems.keySet().iterator();

        if(!it.hasNext()) {
            b.append("    -no items configured-");
        }

        while (it.hasNext()) {
            String itemName = it.next();
            b.append(itemName);
            if (it.hasNext()) {
                b.append(", ");
            }
        }
        b.append("\n");
        b.append(ChatColor.GREEN);
        b.append("------------------------------------------");

        return b.toString();
    }

}
