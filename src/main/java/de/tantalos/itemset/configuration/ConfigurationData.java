package de.tantalos.itemset.configuration;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.ToString;

@XmlRootElement(name = "itemset")
@Getter
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
public class ConfigurationData {
	@XmlElement(name = "item-profile")
	private List<ItemSetData> itemSet;

	@XmlElementWrapper(name="item-definition")
    @XmlElements({
        @XmlElement(name="item", type=ItemData.class),
        @XmlElement(name="item-container", type=ItemContainerData.class)
    })
	private List<ItemData> definedItems;

    @XmlElement(name = "filter")
    private List<PlayerFilter> filter;
}
