package de.tantalos.itemset.configuration;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.bukkit.entity.Player;

import com.google.common.base.Predicates;

import lombok.Getter;
import lombok.ToString;

@XmlSeeAlso(PlayerPredicate.class)
@Getter
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "or")
public class OrPredicate extends PlayerPredicate {

    @XmlElements({ 
            @XmlElement(name = "and", type = AndPredicate.class),
            @XmlElement(name = "or", type = OrPredicate.class), 
            @XmlElement(name = "not", type = NotPredicate.class),
            @XmlElement(name = "is-player-op", type = IsPlayerOp.class),
            @XmlElement(name = "is-player", type = IsPlayerPredicate.class),
            @XmlElement(name = "permission", type = PlayerPermissionPredicate.class),
            @XmlElement(name = "is-in-gamemode", type = IsInGamemode.class)  })
    private List<PlayerPredicate> disjuction;

    @Override
    public boolean apply(Player player) {
        return Predicates.or(disjuction).apply(player);
    }
}
