package de.tantalos.itemset.configuration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.ToString;

@XmlSeeAlso(PlayerPredicate.class)
@Getter
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "is-player-op")
public class IsPlayerOp extends PlayerPredicate {

    @Override
	public boolean apply(Player player) {
		return player.isOp();
	}

}
