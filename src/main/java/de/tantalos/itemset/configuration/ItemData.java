package de.tantalos.itemset.configuration;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.ToString;

@XmlRootElement(name = "item")
@Getter
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemData {

    @XmlID
    @XmlAttribute
    private String id;
    @XmlAttribute
    private String name;
    @XmlAttribute(name = "item-factory")
    private String itemFactoryName;
    @XmlElement(name = "arg")
    private List<String> args;
}
