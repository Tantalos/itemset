package de.tantalos.itemset.configuration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.ToString;

@XmlSeeAlso(PlayerPredicate.class)
@XmlRootElement(name = "filter")
@Getter
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
public class PlayerFilter extends PlayerPredicate {

    @XmlID
    @XmlAttribute
    private String id;
    
    @XmlElements({ 
        @XmlElement(name = "and", type = AndPredicate.class),
        @XmlElement(name = "or", type = OrPredicate.class),
        @XmlElement(name = "not", type = NotPredicate.class),
        @XmlElement(name = "is-player-op", type = IsPlayerOp.class),
        @XmlElement(name = "is-player", type = IsPlayerPredicate.class),
        @XmlElement(name = "permission", type = PlayerPermissionPredicate.class),
        @XmlElement(name = "is-in-gamemode", type = IsInGamemode.class)  })
    private PlayerPredicate playerPredicate;
    
    @Override
    public boolean apply(Player arg0) {
        return playerPredicate.apply(arg0);
    }

}
