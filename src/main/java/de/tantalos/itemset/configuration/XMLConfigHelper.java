package de.tantalos.itemset.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.commons.io.FileUtils;

public class XMLConfigHelper {

    public final static String DEFAULT_CONFIG_NAME = "/config.xml";
    public final static String DEFAULT_DTD_NAME = "/itemset.dtd";
    public final static URL DEFAULT_CONFIG = XMLConfigHelper.class.getResource(DEFAULT_CONFIG_NAME);
    public final static URL DEFAULT_DTD = XMLConfigHelper.class.getResource(DEFAULT_DTD_NAME);

    public final File CONFIG_LOCATION;
    public final File DTD_LOCATION;

    public XMLConfigHelper(String configLoction) {
        CONFIG_LOCATION = new File(configLoction + DEFAULT_CONFIG_NAME);
        DTD_LOCATION = new File(configLoction + DEFAULT_DTD_NAME);
    }

    public XMLConfigHelper() {
        CONFIG_LOCATION = new File("plugins/itemset" + DEFAULT_CONFIG_NAME);
        DTD_LOCATION = new File("plugins/itemset" + DEFAULT_DTD_NAME);
    }

    /**
     * writes default config from jar to run environment if it doesn't exist
     *
     * @throws IOException
     */
    public void saveDefaultConfig() throws IOException {
        CONFIG_LOCATION.getParentFile().mkdirs();
        if (!CONFIG_LOCATION.exists()) {
            FileUtils.copyURLToFile(DEFAULT_CONFIG, CONFIG_LOCATION);
        }

        if (!DTD_LOCATION.exists()) {
            FileUtils.copyURLToFile(DEFAULT_DTD, DTD_LOCATION);
        }
    }

    public void saveConfig(URL source) throws IOException {
        FileUtils.copyURLToFile(source, CONFIG_LOCATION);
    }

    public File loadConfig() {
        return CONFIG_LOCATION;
    }

    public InputStream loadConfigAsStream() throws FileNotFoundException {
        return new FileInputStream(CONFIG_LOCATION);
    }
}
