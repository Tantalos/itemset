package de.tantalos.itemset.configuration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlValue;

import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.ToString;

@XmlSeeAlso(PlayerPredicate.class)
@Getter
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "is-player")
public class IsPlayerPredicate extends PlayerPredicate {

    @XmlValue
    private String player;

    @SuppressWarnings("unused")
    private IsPlayerPredicate() { //for use with reflections
    }
    
    public IsPlayerPredicate(Player player) {
        this.player = player.getUniqueId().toString();
    }
    
    @Override
    public boolean apply(Player arg0) {
        if (arg0 == null) {
            return false;
        }

        return player.equals(arg0.getUniqueId().toString()) || player.equals(arg0.getName());
    }

}
