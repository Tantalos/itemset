package de.tantalos.itemset.configuration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlValue;

import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.ToString;

@XmlSeeAlso(PlayerPredicate.class)
@Getter
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "is-in-gamemode")
public class IsInGamemode extends PlayerPredicate {
    
    @XmlValue
    private int gamemodeNo;

    @SuppressWarnings("unused")
    private IsInGamemode() { //for use with reflections
    }
    
    public IsInGamemode(int gamemodeNo) {
        assert gamemodeNo > -1;
        this.gamemodeNo = gamemodeNo;
    }
    
    @SuppressWarnings("deprecation")
    @Override
    public boolean apply(Player arg0) {
        if (arg0 == null) {
            return false;
        }

        return gamemodeNo == arg0.getGameMode().getValue();
    }

}
