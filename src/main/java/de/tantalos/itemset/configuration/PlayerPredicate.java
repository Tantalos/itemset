package de.tantalos.itemset.configuration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import org.bukkit.entity.Player;

import com.google.common.base.Predicate;

import lombok.Getter;
import lombok.ToString;

@XmlTransient
@Getter
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class PlayerPredicate implements Predicate<Player> {
}
