package de.tantalos.itemset.configuration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.ToString;

@XmlRootElement(name = "item-slot")
@Getter
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemSlotData {
  @XmlAttribute
  private Integer position;
  
  @XmlIDREF
  @XmlAttribute
  private ItemData item;
}
