package de.tantalos.itemset.configuration;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@XmlSeeAlso(ItemData.class)
@Getter
@Setter
@ToString
@XmlRootElement(name = "item-container")
public class ItemContainerData extends ItemData {

    @XmlElement(name = "item-slot")
    private List<ItemSlotData> itemSlots;
}