package de.tantalos.itemset.configuration;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "item-profile")
public class ItemSetData {
    @XmlAttribute
    private String name;
    
    @XmlAttribute
    private String world;
    
    @XmlElement(name = "item-slot")
    private List<ItemSlotData> itemSlots;
    
    @XmlIDREF
    @XmlAttribute(name = "filter")
    private PlayerFilter playerFilter;

}
