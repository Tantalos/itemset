package de.tantalos.itemset.configuration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlValue;

import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.ToString;

@XmlSeeAlso(PlayerPredicate.class)
@Getter
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "permission")
public class PlayerPermissionPredicate extends PlayerPredicate {

	@XmlValue
	private String permission;
	
	@SuppressWarnings("unused")
	private PlayerPermissionPredicate() { //for use with reflections
	}
	
	
	public PlayerPermissionPredicate(String permission) {
		this.permission = permission;
	}

	@Override
	public boolean apply(Player player) {
		return player.hasPermission(permission);
	}

}
