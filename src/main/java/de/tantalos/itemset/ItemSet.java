package de.tantalos.itemset;

import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.Plugin;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import de.tantalos.eventlistener.EventCallback;
import de.tantalos.eventlistener.PlayerTeleportEventListener;
import de.tantalos.itemset.connector.ItemBuilder;

public class ItemSet {

    private final World world;
    private final Plugin plugin;
    private final Map<Integer, ItemBuilder> itemDefinition;
    private final Predicate<Player> playerPredicate;
    private PlayerTeleportEventListener eventListener;

    ItemSet(Plugin plugin, World world, Predicate<Player> playerPredicate,
            Map<Integer, ItemBuilder> itemDefinition) {
        Preconditions.checkNotNull(itemDefinition);
        Preconditions.checkNotNull(playerPredicate);

        this.plugin = plugin;
        this.world = world;
        this.playerPredicate = playerPredicate;
        this.itemDefinition = itemDefinition;
    }

    /**
     * sets items to all players who are in the predefined world and full fill
     * predefined predicate
     */
    public void setItemsToAll() {
        for (Player player : plugin.getServer().getOnlinePlayers()) {
            if (world.equals(player.getLocation().getWorld())) {
                if (playerPredicate.apply(player)) {
                    setItemsToPlayer(player);
                }
            }
        }
    }

    private void setItemsToPlayer(Player player) {
        PlayerInventory playerInv = player.getInventory();
        playerInv.clear();
        for (Entry<Integer, ItemBuilder> itemDef : itemDefinition.entrySet()) {
            Integer itemSlot = itemDef.getKey();
            ItemStack item = itemDef.getValue().getItem();

            if (itemSlot < 36) { // inventory slot
                playerInv.setItem(itemSlot, item);
            } else if (itemSlot < 40) { // armor slot
                switch (itemSlot) {
                case 36:
                    playerInv.setHelmet(item);
                    break;
                case 37:
                    playerInv.setChestplate(item);
                    break;
                case 38:
                    playerInv.setLeggings(item);
                    break;
                case 39:
                    playerInv.setBoots(item);
                    break;
                default:
                    assert false;
                }
            } else if (itemSlot < 41) { // off hand
                playerInv.setItemInOffHand(item);
            }
        }
    }

    /**
     * Starts a listener which sets the items to all players who are teleported
     */
    public void reloadItemsAfterTeleport() {
        if (eventListener != null) {
            eventListener.destroy();
        }

        eventListener = new PlayerTeleportEventListener(plugin, Predicates.alwaysTrue(),
                new EventCallback<PlayerTeleportEvent>() {

            @Override
            public void onEvent(PlayerTeleportEvent event) {
                if (!playerPredicate.apply(event.getPlayer())) {
                    return;
                }

                // clear inventory. this will avoid to steal items for
                // other worlds
                if (world.equals(event.getFrom().getWorld())) {
                    event.getPlayer().getInventory().clear();
                }

                if (world.equals(event.getTo().getWorld())) {
                    setItemsToPlayer(event.getPlayer());
                }

                return;
            }
        });
    }

    public void undetermineWorldItem() {
        if (eventListener != null) {
            eventListener.destroy();
        }
    }
}
