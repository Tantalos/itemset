package de.tantalos.itemset;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.google.common.base.Optional;

import de.tantalos.itemset.configuration.ItemContainerData;
import de.tantalos.itemset.configuration.ItemData;
import de.tantalos.itemset.configuration.ItemSlotData;
import de.tantalos.itemset.connector.IllegalConfigurationException;
import de.tantalos.itemset.connector.ItemBuilder;
import de.tantalos.itemset.connector.ItemContainerFactory;
import de.tantalos.itemset.connector.ItemFactory;

/**
 * Creates item factories during runtime via reflection
 *
 */
public class ItemFactoryCreator {
    static Logger logger = Logger.getLogger("itemset");

    public ItemFactoryCreator() {
    }

    /**
     * Creates an ItemFactory if the nested items exist in the item pool. The
     * creation of ItemFactory will fail if nested items are needed but not
     * (initialized) in item pool. If the creation fails otherwise this method
     * will throw an exception.
     *
     *
     * @param item
     *            - specification for item
     * @param itemPool
     * @throws RuntimeException
     *             if creation of item fails
     * @throws IllegalConfigurationException
     *             if the configuration arguments are not feasible
     * @return Optional<ItemBuilder> optional with ItemFactory if it could be
     *         created, optional without ItemFactory if it could't be created.
     *
     */
    public Optional<ItemFactory<? extends ItemBuilder>> createItemFactoryUsePool(ItemData item,
            Map<String, ItemBuilder> itemPool)
            throws IllegalConfigurationException, RuntimeException {

        /*
         * nested Items might not be created yet
         */

        String factoryClass = item.getItemFactoryName();

        // Add nested items to factory
        List<ItemBuilder> nestedItemBuilder = new ArrayList<ItemBuilder>();
        if (item instanceof ItemContainerData) {
            ItemContainerData itemContainer = (ItemContainerData) item;
            for (ItemSlotData nestedItemSlot : itemContainer.getItemSlots()) {
                String itemId = nestedItemSlot.getItem().getId();
                ItemBuilder nestedItem = itemPool.get(itemId);
                if (nestedItem == null) {
                    return Optional.<ItemFactory<? extends ItemBuilder>>absent();
                }
                nestedItemBuilder.add(nestedItem);
            }
        }

        try {
            ItemFactory<? extends ItemBuilder> itemFacotry = createItemFactoryViaReflection(factoryClass,
                    nestedItemBuilder);
            return Optional.<ItemFactory<? extends ItemBuilder>>of(itemFacotry);

        } catch (ReflectiveOperationException e) {
            logger.severe("Item can not be created due to reflection error. Check if plugin is installed");
            throw new RuntimeException(e);
        }
    }


    /**
     * Returns a factory which builds an item builder. If the factory creation
     * of given arguments fails, factory will be returned any way, which creates
     * returns error feedback
     *
     * @param factoryClass
     *            - the factory class name
     * @param nestedItems
     *            - nested item Builders which. Can be null
     * @return
     */
    static ItemFactory<? extends ItemBuilder> createItemFactoryViaReflection(String factoryClass,
            List<? extends ItemBuilder> nestedItems) throws IllegalConfigurationException, ReflectiveOperationException {
        ItemFactory<? extends ItemBuilder> itemFac = null;

		@SuppressWarnings("unchecked")
		Class<? extends ItemFactory<? extends ItemBuilder>> clazz = (Class<? extends ItemFactory<? extends ItemBuilder>>) Class
				.forName(factoryClass);

		// handle factory container
		if (ItemContainerFactory.class.isAssignableFrom(clazz)) {
			itemFac = getItemContainerInstance(clazz, nestedItems);
		} else {
			itemFac = clazz.getConstructor().newInstance();
		}

        assert itemFac != null;
        return itemFac;
    }

    static ItemContainerFactory<? extends ItemBuilder> getItemContainerInstance(
            Class<? extends ItemFactory<? extends ItemBuilder>> clazz, List<? extends ItemBuilder> constructorParam)
            throws ReflectiveOperationException, IllegalConfigurationException {

        @SuppressWarnings("unchecked")
        Class<? extends ItemContainerFactory<? extends ItemBuilder>> containerClazz = (Class<? extends ItemContainerFactory<? extends ItemBuilder>>) clazz;

        // verify type conformity
        Class<?> parameterClass = (Class<?>) ((ParameterizedType)containerClazz.getGenericSuperclass()).getActualTypeArguments()[0];
        for (ItemBuilder item : constructorParam) {
            if (!parameterClass.isAssignableFrom(item.getClass())) {
                throw new IllegalConfigurationException("Illegal Configuration. The container " + containerClazz
                        + " needs nested elements of type " + parameterClass);
            }
        }

        ItemContainerFactory<? extends ItemBuilder> itemFactoryContainer = containerClazz.getConstructor(List.class)
                .newInstance(constructorParam);

        return itemFactoryContainer;
    }


}
