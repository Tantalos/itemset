package de.tantalos.itemset;

import java.io.IOException;
import java.util.Map;

import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

import de.tantalos.itemset.configuration.XMLConfigHelper;
import de.tantalos.itemset.connector.ItemBuilder;

public class Main extends JavaPlugin {

    public static JavaPlugin plugin;
    public static int BukkitVersion;

    XMLConfigHelper config;

    ItemSetManager itemSetManager;



	@Override
	public void onDisable() {
		super.onDisable();
	}

	@Override
	public void onEnable() {
		super.onEnable();
        plugin = this;
        config = new XMLConfigHelper();
        try {
            config.saveDefaultConfig();
        } catch (IOException e) {
            e.printStackTrace();
        }
        itemSetManager = new ItemSetManager(this);
        itemSetManager.createWorldSetFromConfig(config);

        Map<String, ItemBuilder> items = itemSetManager.getItems();
        PluginCommand c = getCommand("itemset");
        c.setExecutor(new ItemSetCommands(items, getServer()));
	}

    @Override
    public void reloadConfig() {
        super.reloadConfig();
        config = new XMLConfigHelper();

        itemSetManager.destoryItemSets();
        itemSetManager.createWorldSetFromConfig(config);
    }


}
