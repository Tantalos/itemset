package de.tantalos.itemset;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import de.tantalos.itemset.configuration.ConfigurationData;
import de.tantalos.itemset.configuration.ItemData;
import de.tantalos.itemset.configuration.ItemSetData;
import de.tantalos.itemset.configuration.ItemSlotData;
import de.tantalos.itemset.configuration.XMLConfigHelper;
import de.tantalos.itemset.connector.IllegalConfigurationException;
import de.tantalos.itemset.connector.ItemBuilder;
import de.tantalos.itemset.connector.ItemFactory;
import de.tantalos.itemset.utils.StaticItem;

public class ItemSetManager {

    /**
     * Default Error items
     */
    private static final ItemBuilder ERROR_IN_COFNIG = new StaticItem(Material.BARRIER,
            ChatColor.RED + "Error while loading item", "Check your configuration!");
    private static final ItemBuilder ERROR_IN_PLUGIN = new StaticItem(Material.BARRIER,
            ChatColor.RED + "Error in thirdparty plugin occured. Is it up to date?",
            "A plugin which you are using failed! Check if factory is set correct or if plugin is up to date!");
    private final Logger logger = Logger.getLogger("itemset");

    private final Set<ItemSet> worldSets = Sets.newHashSet();
    private final Map<String, ItemBuilder> itemPool = Maps.newHashMap();
    private final Set<ItemData> unableToCreate = Sets.newHashSet();

    private final Server server;
    private final Plugin plugin;

    private final ItemFactoryCreator reflectionFactoryBuilder = new ItemFactoryCreator();


    public ItemSetManager(Plugin plugin) {
        this.server = plugin.getServer();
        this.plugin = plugin;
    }

    /**
     * Creates configured items and fixes them to the item slot
     *
     * @param config
     *            configuration
     */
    public Set<ItemSet> createWorldSetFromConfig(XMLConfigHelper config) {
        try {
            ConfigurationData itemSetConfig = loadConfigFrom(config);

            createAllItemBuilder(itemSetConfig);
            createItemSets(itemSetConfig);

        } catch (IllegalConfigurationException e) {
            e.printStackTrace();
        }
        return worldSets;
    }

    void createAllItemBuilder(ConfigurationData itemSetConfig) throws IllegalConfigurationException {
        /*
         * Try to create queued item. If not possible add to queue again. Abort
         * if all remaining items in queue could not be created successively.
         */
        final Queue<ItemData> queue = Lists.newLinkedList(itemSetConfig.getDefinedItems());
        final Map<String, ItemBuilder> tempItemPool = Maps.newHashMap();
        while (!queue.isEmpty()) {

            /* checks if the left items can be created */
            if (unableToCreate.containsAll(queue)) {
                throw new IllegalConfigurationException(
                        "Configuration is not consistent. Items needed from pool cannot be created");
            }

            ItemData itemData = queue.poll();
            Optional<ItemBuilder> item = createItemsUsePool(itemData, tempItemPool);

            if (item.isPresent()) {
                tempItemPool.put(itemData.getId(), item.get());

                /*
                 * An item could be created. the other items can possibly be
                 * initialized now
                 */
                unableToCreate.clear();
            } else {
                queue.add(itemData);
                unableToCreate.add(itemData);
            }
        }
        itemPool.putAll(tempItemPool);
    }

    /**
     * create item sets
     *
     * @param itemSetConfig
     *            config
     */
    private void createItemSets(ConfigurationData itemSetConfig) {
        for (ItemSetData itemSetData : itemSetConfig.getItemSet()) {
            ItemSet itemSet = createItemSets(itemSetData);
            itemSet.setItemsToAll();
            itemSet.reloadItemsAfterTeleport();
            worldSets.add(itemSet);
        }
    }

    /**
     * Creates ItemBuilder if the nested items exist in the item pool. The
     * creation of item builder will fail if nested items are needed but not
     * (initialized) in item pool. Returns default (Error-)ItemBuilder if an
     * unexpected error occurs.
     *
     * @param item
     *            initialization data from the item
     * @param itemPool
     *            pool of initialized items which can be used to set nested
     *            items
     * @return returns optional with ItemBuilder if item could be created,
     *         optional without ItemBuilder if it could't be created.
     */
    private Optional<ItemBuilder> createItemsUsePool(ItemData item, Map<String, ItemBuilder> itemPool) {
        ItemBuilder itembuilder;
        try {

            Optional<ItemFactory<? extends ItemBuilder>> itemFactory = reflectionFactoryBuilder
                    .createItemFactoryUsePool(item, itemPool);
            if (!itemFactory.isPresent()) {
                return Optional.absent();
            }

            String[] args = listToArray(item.getArgs());
            itembuilder = itemFactory.get().buildItemDefinition(args);
        } catch (IllegalConfigurationException e) {
            itembuilder = ERROR_IN_COFNIG;
            e.printStackTrace();
        } catch (Throwable e) {
            itembuilder = ERROR_IN_PLUGIN;
            e.printStackTrace();
        }

        return Optional.of(itembuilder);
    }

    private static String[] listToArray(List<String> list) {
        if (list == null) {
            return new String[0];
        }
        String[] listElements = new String[list.size()];
        list.toArray(listElements);
        return listElements;
    }

    private ItemSet createItemSets(ItemSetData itemSetData) {
        ItemSetBuilder itemSet = new ItemSetBuilder(plugin);

        World world = server.getWorld(itemSetData.getWorld());
        itemSet.setWorld(world);
        itemSet.setPlayerPredicate(itemSetData.getPlayerFilter());

        for (ItemSlotData itemSlot : itemSetData.getItemSlots()) {
            int position = itemSlot.getPosition();
            String itemId = itemSlot.getItem().getId();

            ItemBuilder item = itemPool.get(itemId);
            if (position > 40 || position < 0 || item == null) {
                logger.severe("Illegal inventory slot. Item must be in slot from 0-40");
            }
            itemSet.fixSlot(position, item);
        }
        return itemSet.build();
    }

    public ConfigurationData loadConfigFrom(XMLConfigHelper config) throws IllegalConfigurationException {
        ConfigurationData itemSetConfig = null;

        try {
            InputStream is = config.loadConfigAsStream();
            JAXBContext jaxbContext = JAXBContext.newInstance(ConfigurationData.class);
            XMLInputFactory xif = XMLInputFactory.newFactory();
            XMLStreamReader xsr = xif.createXMLStreamReader(new StreamSource(is));

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            itemSetConfig = (ConfigurationData) jaxbUnmarshaller.unmarshal(xsr);
        } catch (JAXBException | XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }

        return itemSetConfig;
    }

    public void destoryItemSets() {
        worldSets.forEach(set -> set.undetermineWorldItem());
        worldSets.clear();
        itemPool.clear();
        unableToCreate.clear();
    }

    public Map<String, ItemBuilder> getItems() {
        return Maps.newHashMap(itemPool);
    }

    public Set<ItemSet> getAllItemSets() {
        return Sets.newHashSet(worldSets);
    }

}
