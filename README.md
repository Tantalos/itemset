##Summary

Item Set is a lightweight and generic item assembly tool. It supports a slim configuration for items a player gets, when a world is entered. This can be used to easily define items (with custom behaviour) which a player gets in a lobby.



##Content
1. [Simple Configuration](https://bitbucket.org/Tantalos/itemset/wiki/Home#simple-configuration)
2. [Filters](https://bitbucket.org/Tantalos/itemset/wiki/Filter)
2. [Make your item support ItemSet](https://bitbucket.org/Tantalos/itemset/wiki/Develop)

 
##Simple Configuration

**Note:** I recommend to use an xml editor for editing the configuration. There is a dtd file which helps to write viable configuration. (e.g. notepad++ with XMLTools Plugin)

You can create profiles for a specific world. A profile is a template for items constellation in the players inventory, which a player gets after he joins the specified world. The profile and items are defined in XMl.


###Profile

The `world="myWorld"` attribute determines the world in which items will be replaced.

```
#!XML

<itemset>
    <item-profile world="world" name="any profile name">
        ... define your items slots here
    </item-profile>
<itemset>
```


###Items

Secondly the items must be defined. An item needs an unique id and an implementation of `ItemFactory.class` which is able to create an item. Item factories can take string arguments and depend on factory implementation. (The factory must be provided by the plugin developer)

**For example:** To create a normal item without any functionallity you can use the `de.tantalos.itemset.utils.StaticItemFactory`. It takes two optional arguments for the material and amount of items in stack.



```
#!XML
<item-definition>
    <item id="normal stick" item-factory="de.tantalos.itemset.utils.StaticItemFactory.class">
        <arg>material=STICK</arg>
        <arg>amount=2</arg>
    </item>
    <item id="other item" item-factory="de.tantalos.itemset.utils.StaticItemFactory.class"/>
</item-definition>
```



###Container Items
Container items are special items which contains other items. An container item needs an unique id and an implementation of `ItemContainerFactory.class` which is able to create an container item. Other items can be added as elements of container item. Item container factories can take string arguments and depend on factory implementation. (The factory must be provided by the plugin developer)

**For example:** To create a item which is choosen randomly from an item pool you can use the `de.tantalos.itemset.utils.RandomItemFactory`. It takes no arguments.


```
#!XML
<item-definition>
    <item-container id="random" name="random" item-factory="de.tantalos.itemset.utils.RandomItemFactory">
        <item-slot item="item0" position="0" />
        <item-slot item="item1" position="0" />
        <item-slot item="item2" position="0" />
    </item-container>

    <item id="item0" item-factory="de.tantalos.itemset.utils.StaticItemFactory" />
    <item id="item1" item-factory="de.tantalos.itemset.utils.StaticItemFactory" >
        <arg>material=BLAZE_ROD</arg>
        <arg>amount=3</arg>
        <arg>text=stick of truth</arg>
    </item>
    <item id="item2" item-factory="de.tantalos.itemset.utils.StaticItemFactory">
        <arg>material=REDSTONE_ORE</arg>
        <arg>amount=3</arg>
    </item>
</item-definition>
```



This framework supports the definition for more complex items like an clickable inventory guis or items with custom properties (e.g. see my other projects [InventoryGUI](https://bitbucket.org/Tantalos/iventorygui), [FunGun](https://bitbucket.org/Tantalos/fungun) etc.). Actually items from external plugins can be defined this way, as long the plugins provides an implementation of `ItemFacotry.class`.


###Link item to profile

Once there are items defined, they can be added to a profile. An item-slot consists of an item (given via item id of defined items) `item="normal stick"` and position in the inventory `position="8"`.


```
#!XML

<itemset>
    <item-profile world="world">
        <item-slot item="normal stick" position="8" />
    </item-profile>

    <item-definition>
        <item id="normal stick" name="normal stick" item-factory="de.tantalos.itemset.utils.StaticItemFactory.class">
            <arg>material=STICK</arg>
            <arg>amount=2</arg>
        </item>
    </item-definition>
<itemset>
```

#Example with external plugin [FunGun](https://bitbucket.org/Tantalos/fungun)

You need to add the fungun plugin to your folder plugins folder. FunGun provides the factory `de.tantalos.fungun.itemSet.FunGunFactory`

The configuration looks like

```
#!XML

<itemset>
    <item-profile world="world" name="full_profile">
        <item-slot item="minimal-fungun" position="8" />
        <item-slot item="fungun" position="7" />
        <item-slot item="static-item" position="6" />
    </item-profile>


    <item-definition>
        <item id="minimal-fungun" name="FunGun" item-factory="de.tantalos.fungun.itemSet.FunGunFactory" />
        <item id="fungun" name="FunGun" item-factory="de.tantalos.fungun.itemSet.FunGunFactory">
            <arg>effect=LAVA_POP</arg>
            <arg>effect=HEART</arg>
            <arg>sound=ENTITY_CAT_AMBIENT</arg>
            <arg>material=STICK</arg>
        </item>
        <item id="static-item" name="FunGun" item-factory="de.tantalos.itemset.utils.StaticItemFactory.class">
            <arg>material=BLAZE_ROD</arg>
        </item>
    </item-definition>
</itemset>

```