## [0.0.6-SNAPSHOT] - 2017-03-30
### Added
- utility to create invisible string identification for items

## [0.0.5-SNAPSHOT] - 2017-03-19

### Added
- added command /itemset show
- added command /itemset give <item name> [player name]
- added permissions for command

### Fixed
- configuration of ContainerItems which are referenced reciprocal wont freeze but throw exception


## [0.0.4-SNAPSHOT] - 2017-03-10
### Changed
- repository for dependencies
- configuration outside of jar

## [0.0.3-SNAPSHOT] - 2017-03-06
### Added
- player filter for item profiles
- README and wiki

### Changed
- dtd and xml structure for configuration